import Vue from 'vue'
import Vuex from 'vuex'

import app from '@/feathers-client'

Vue.use(Vuex)

export default new Vuex.Store({

   state: {
      todos: undefined,
   },

   mutations: {
      CREATE_TODO: function(state, todo) {
         if (state.todos === undefined) state.todos = {}
         Vue.set(state.todos, todo.id, todo)
      },
      PATCH_TODO: function(state, todo) {
         Vue.set(state.todos, todo.id, todo)
      },
      DELETE_TODO: function(state, todo) {
         Vue.delete(state.todos, todo.id)
      },
   },

   actions: {
      FETCH_TODOS: async function({ commit }) {
         let todoList = await app.service('todos').find()
         todoList.forEach(todo => {
            commit('CREATE_TODO', todo)
         })
      },
   }
})
